FROM python:3.6
ENV PYTHONUNBUFFERED 1

COPY . /app
WORKDIR /app

RUN apt-get update
RUN apt-get install -y binutils libproj-dev gdal-bin
RUN pip install pipenv
RUN pipenv install --system