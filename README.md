## PDVs

### Local environment

Use `docker-compose` to get things up and running quickly. [Install it](https://docs.docker.com/compose/install/), then run the following commands to create a local container, and load the test data:  
```shell
docker-compose build
docker-compose run --rm web ./manage.py migrate
docker-compose run --rm web ./manage.py load_test_data
docker-compose up

```
The application should be running at [http://localhost:8000/](http://localhost:8000/).  
Run the test suite with:
```shell
docker-compose run --rm web ./manage.py test

```

### Deploy

The project is setup to be deployed to heroku. To create an app and enable the PostGIS libraries, run the following commands:  

```shell
heroku create <app_name>
heroku config:set BUILD_WITH_GEO_LIBRARIES=1
heroku addons:add heroku-postgresql
heroku pg:psql -c "create extension postgis;"
heroku run ./manage.py migrate
```

### API endpoints

You can checkout the API docs at [http://localhost:8000/docs/](http://localhost:8000/docs/).  

#### POST /pdvs/
Creates a new call record. This endpoint accepts both `start` and `end` records.
+ body:

        {
          "tradingName": "Poly1",
          "ownerName": "Diego Ponci",
          "document": "123.456.789-00",
          "coverageArea": {
             "type": "MultiPolygon",
             "coordinates": [
                [
                   [
                      [1.0, 5.5],
                      [2.0, 5.5],
                      [2.0, 3.5],
                      [1.0, 3.5],
                      [1.0, 5.5],
                   ]
                ]
             ]
          },
          "address": {
             "type": "Point",
             "coordinates": [
                1.597337,
                4.713538
             ]
          }
       }
+ Response 201 (application/json).

#### GET /pdvs/<id>/
Retrieves a PDV using its `id`.
+ Parameters
  + id: (required) integer - The unique identifier of the PDV
+ example of retrieval:
    ```
    curl -X GET http://localhost:8000/pdvs/1/
    ```
+ Response 200 (application/json). E.g.:
         
        {
          "id": 1,
          "tradingName": "Poly1",
          "ownerName": "Diego Ponci",
          "document": "123.456.789-00",
          "coverageArea": {
             "type": "MultiPolygon",
             "coordinates": [
                [
                   [
                      [1.0, 5.5],
                      [2.0, 5.5],
                      [2.0, 3.5],
                      [1.0, 3.5],
                      [1.0, 5.5],
                   ]
                ]
             ]
          },
          "address": {
             "type": "Point",
             "coordinates": [
                1.597337,
                4.713538
             ]
          }
       }

#### GET /pdvs/search/<lat>,<lng>/
Retrieves the nearest PDV where the given `lat` and `lng` coordinates are covered.
+ Parameters
  + lat-lng: (required) numbers - Comma separated coordinates
+ example of retrieval:
    ```
    curl -X GET http://localhost:8000/pdvs/search/-47.02279,-22.90013/
    ```
+ Response 200 (application/json). E.g.:
         
         {
          "id": 1,
          "tradingName": "Poly1",
          "ownerName": "Diego Ponci",
          "document": "123.456.789-00",
          "coverageArea": {
             "type": "MultiPolygon",
             "coordinates": [
                [
                   [
                      [1.0, 5.5],
                      [2.0, 5.5],
                      [2.0, 3.5],
                      [1.0, 3.5],
                      [1.0, 5.5],
                   ]
                ]
             ]
          },
          "address": {
             "type": "Point",
             "coordinates": [
                1.597337,
                4.713538
             ]
          }
       }

To visually assist with the search tests, I used leaflet to load simple and the given GeoJSON data, and interact with the PDVs.  
The test with the simple data I wrote is located at [geo_pdvs/pdvs/tests/simple.html](geo_pdvs/pdvs/tests/simple.html), and the test with the given JSON is located at [geo_pdvs/pdvs/tests/pdvs.html](geo_pdvs/pdvs/tests/pdvs.html).  