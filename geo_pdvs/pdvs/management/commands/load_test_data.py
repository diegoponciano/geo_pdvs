from django.core.management.base import BaseCommand
# from django.core.management.base import CommandError

from geo_pdvs.pdvs.tests.utils import load_test_data
from geo_pdvs.pdvs.models import PDV


class Command(BaseCommand):
    help = 'Loads GeoJSON PDVs from the provided data'

    def add_arguments(self, parser):
        parser.add_argument(
            '--clear',
            action='store_true',
            dest='delete',
            help='Delete all existing PDVs',
        )

    def handle(self, *args, **options):
        try:
            self.stdout.write('Loading PDVs...')
            if options['delete']:
                PDV.objects.all().delete()
            load_test_data()
            self.stdout.write(self.style.SUCCESS(
                'Sucessfully loaded all PDVs.'))
        except Exception as e:
            # raise CommandError('Poll "%s" does not exist' % poll_id)
            self.stdout.write(self.style.ERROR(e))
