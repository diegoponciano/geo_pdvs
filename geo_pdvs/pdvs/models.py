from django.contrib.gis.db import models


class PDV(models.Model):
    trading_name = models.CharField(max_length=255)
    owner_name = models.CharField(max_length=255)
    document = models.CharField(max_length=200, unique=True)
    coverage_area = models.MultiPolygonField()
    address = models.PointField()
