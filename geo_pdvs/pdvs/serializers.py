from rest_framework import serializers
from rest_framework_gis.fields import GeometryField


from .models import PDV


class PDVSerializer(serializers.ModelSerializer):
    tradingName = serializers.CharField(source='trading_name')
    ownerName = serializers.CharField(source='owner_name')
    coverageArea = GeometryField(source='coverage_area')

    class Meta:
        model = PDV
        fields = ['id', 'tradingName', 'ownerName', 'document',
                  'coverageArea', 'address']
