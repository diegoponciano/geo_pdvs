var geodata = {
    "pdvs": [
       {
          "tradingName": "Poly1",
          "ownerName": "Diego Ponci",
          "document": "123.456.789-00",
          "coverageArea": {
             "type": "MultiPolygon",
             "coordinates": [
                [
                   [
                      [1.0, 5.5],
                      [2.0, 5.5],
                      [2.0, 3.5],
                      [1.0, 3.5],
                      [1.0, 5.5],
                   ]
                ]
             ]
          },
          "address": {
             "type": "Point",
             "coordinates": [
                1.597337,
                4.713538
             ]
          }
       },
       {
          "tradingName": "Poly2",
          "ownerName": "Diego Ponci",
          "document": "123.456.789-00",
          "coverageArea": {
             "type": "MultiPolygon",
             "coordinates": [
                [
                   [
                      [1.0, 6],
                      [1.5, 6],
                      [1.5, 4],
                      [1.0, 4],
                      [1.0, 6],
                   ]
                ]
             ]
          },
          "address": {
             "type": "Point",
             "coordinates": [
                1.297337,
                5.013538
             ]
          }
       },
       {
          "tradingName": "Poly3",
          "ownerName": "Diego Ponci",
          "document": "123.456.789-00",
          "coverageArea": {
             "type": "MultiPolygon",
             "coordinates": [
                [
                   [
                      [1.0, 5],
                      [1.5, 5],
                      [1.5, 3],
                      [1.0, 3],
                      [1.0, 5],
                   ]
                ]
             ]
          },
          "address": {
             "type": "Point",
             "coordinates": [
                1.297337,
                3.944092
             ]
          }
       },
    ]
 }
