import json
from django.contrib.gis.geos import GEOSGeometry
from model_mommy import mommy
from rest_framework.test import APITestCase

from ..models import PDV
from .utils import load_test_data


def point(p1, p2):
    return GEOSGeometry(json.dumps({
        "type": "Point",
        "coordinates": [p1, p2]
    }))


def multi(array):
    return GEOSGeometry(json.dumps({
        "type": "MultiPolygon",
        "coordinates": [[array]]
    }))


class TestPDVSearchSimpleData(APITestCase):
    def setUp(self):
        PDV.objects.all().delete()
        self.pdv1 = mommy.make(PDV, coverage_area=multi([
            [1.0, 5.5],
            [2.0, 5.5],
            [2.0, 3.5],
            [1.0, 3.5],
            [1.0, 5.5],
        ]), address=point(1.59, 4.71), trading_name='p1')
        self.pdv2 = mommy.make(PDV, coverage_area=multi([
            [1.0, 6],
            [1.5, 6],
            [1.5, 4],
            [1.0, 4],
            [1.0, 6],
        ]), address=point(1.29, 5.01), trading_name='p2')
        self.pdv3 = mommy.make(PDV, coverage_area=multi([
            [1.0, 5],
            [1.5, 5],
            [1.5, 3],
            [1.0, 3],
            [1.0, 5],
        ]), address=point(1.29, 3.94), trading_name='p3')

    def test_closer_to_pdv3_but_inside_only_pdv1(self):
        response = self.client.get('/pdvs/search/%s,%s/' % (
            1.62, 3.89))
        self.assertEqual(response.data['tradingName'], 'p1')

    def test_closer_to_pdv3_inside_all_intersections(self):
        response = self.client.get('/pdvs/search/%s,%s/' % (
            1.45, 4.13))
        self.assertEqual(response.data['tradingName'], 'p3')

    def test_get_covered_PDV_makes_only_one_query(self):
        with self.assertNumQueries(1):
            self.client.get('/pdvs/search/%s,%s/' % (
                1.45, 4.13))


class TestPDVSearchProvidedData(APITestCase):
    def setUp(self):
        PDV.objects.all().delete()
        load_test_data()

    def test_undefined_point(self):
        self.response = self.client.get('/pdvs/search/,/')
        self.assertEqual(self.response.status_code, 404)

    def test_correct_response(self):
        self.response = self.client.get('/pdvs/search/%s,%s/' % (
            -47.02279, -22.90013))
        self.assertEqual(self.response.status_code, 200)
        self.assertIsNotNone(self.response.data['id'])

    def test_get_outside_all_pdvs(self):
        self.response = self.client.get('/pdvs/search/%s,%s/' % (
            -44.02279, -25.90013))
        self.assertEqual(self.response.status_code, 404)
        self.assertIsNone(self.response.data.get('id'))
