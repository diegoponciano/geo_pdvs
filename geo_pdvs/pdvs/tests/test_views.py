from model_mommy import mommy
from rest_framework.test import APITestCase

from ..models import PDV


class TestPDVGet(APITestCase):
    def setUp(self):
        self.pdv = mommy.make(PDV)
        self.response = self.client.get('/pdvs/%s/' % self.pdv.id)

    def test_correct_response_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_response_body(self):
        self.assertEqual(
            self.response.data['tradingName'], self.pdv.trading_name)


class TestPDVCreateView(APITestCase):
    def setUp(self):
        self.data = {
            "tradingName": "Adega da Cerveja - Pinheiros",
            "ownerName": "Zé da Silva",
            "document": "1432132123891/0001",
            "coverageArea": {
                "type": "MultiPolygon",
                "coordinates": [
                    [[[30, 20], [45, 40], [10, 40], [30, 20]]],
                    [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
                ]
            },
            "address": {
                "type": "Point",
                "coordinates": [-46.57421, -21.785741]
            }
        }
        self.response = self.client.post('/pdvs/', self.data, format='json')

    def test_correct_response_code(self):
        self.assertEqual(self.response.status_code, 201)

    def test_response_body(self):
        self.assertEqual(
            self.response.data['tradingName'], self.data['tradingName'])

    def test_address_point(self):
        instance = PDV.objects.get(id=self.response.data['id'])
        self.assertEqual(instance.address.tuple, (-46.57421, -21.785741))
