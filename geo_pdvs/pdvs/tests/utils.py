import json
from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry
from pathlib import Path

from ..models import PDV


def load_test_data():
    json_path = Path(
        settings.BASE_DIR, 'geo_pdvs', 'pdvs', 'tests', 'pdvs.json')
    with open(json_path) as json_file:
        pdvs = []
        data = json.load(json_file)
        for pdv in data['pdvs']:
            pdvs.append(PDV(
                trading_name=pdv['tradingName'],
                owner_name=pdv['ownerName'],
                document=pdv['document'],
                coverage_area=GEOSGeometry(str(pdv['coverageArea'])),
                address=GEOSGeometry(str(pdv['address'])),
            ))
        PDV.objects.bulk_create(pdvs)
