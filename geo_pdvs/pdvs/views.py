from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.http import Http404
from rest_framework import generics
from rest_framework.response import Response

from .models import PDV
from .serializers import PDVSerializer


class CreatePDV(generics.CreateAPIView):
    queryset = PDV.objects.all()
    serializer_class = PDVSerializer


class GetPDV(generics.RetrieveAPIView):
    queryset = PDV.objects.all()
    serializer_class = PDVSerializer


class SearchPDV(generics.RetrieveAPIView):
    queryset = PDV.objects.all()
    serializer_class = PDVSerializer

    def retrieve(self, *args, **kwargs):
        instance = self.get_queryset().first()
        if instance is None:
            raise Http404
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def get_queryset(self):
        lat = self.kwargs.get('lat')
        lng = self.kwargs.get('lng')
        if lat and lng:
            point = Point(float(lat), float(lng))
            return PDV.objects.filter(coverage_area__contains=point).annotate(
                distance=Distance('address', point)
            ).order_by('distance')
        else:
            return PDV.objects.none()
